resource "aws_security_group" "public-sg" {
    name = "vpc_web"
    description = "Allow incoming HTTP connections."

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 3000
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = aws_vpc.default.id

  tags = {
    Name = "public-sg"
  }
}

resource "aws_instance" "node-api" {
    ami = "ami-07c1207a9d40bc3bd"
    availability_zone = "us-east-2a"
    instance_type = "t2.micro"
    key_name = var.aws_key_name
    vpc_security_group_ids = [aws_security_group.public-sg.id]
    subnet_id = aws_subnet.public.id
    associate_public_ip_address = true
    source_dest_check = false
  user_data = <<EOF
#!/bin/bash
export APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1
apt-get -y install apt-transport-https ca-certificates curl software-properties-common 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add - 
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" 
apt update 
apt-cache policy docker-ce 
apt-get -y install docker-ce 
usermod -aG docker ubuntu
EOF

  tags = {
    Name = "node-api-04"
  }
}

resource "aws_eip" "node-api" {
    instance = aws_instance.node-api.id
    vpc = true
}
