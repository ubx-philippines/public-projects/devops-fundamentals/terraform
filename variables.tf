variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_path" {}
variable "aws_key_name" {}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-east-2"
}
variable "amis" {
    description = "AMIs by region"
    default = {
        us-east-2 = "ami-07c1207a9d40bc3bd" # ubuntu 18.04 LTS
    }
}
variable "vpccidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.1.0/24"
}

