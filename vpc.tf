resource "aws_vpc" "default" {
  cidr_block = var.vpccidr
  enable_dns_hostnames = true

  tags = {
    Name = "2nd vpc"
  }
}

resource "aws_internet_gateway" "default" {
    vpc_id = aws_vpc.default.id
}


/*
  Public Subnet
*/
resource "aws_subnet" "public" {
    vpc_id = aws_vpc.default.id

    cidr_block = var.public_subnet_cidr
    availability_zone = "us-east-2a"

  tags = {
    Name = "publci subnet"
  }
}

resource "aws_route_table" "us-east-2a" {
    vpc_id = aws_vpc.default.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.default.id
    }

  tags = {
    Name = "2nd route table"
  }
}

resource "aws_route_table_association" "us-east-2a-public-sg" {
    subnet_id = aws_subnet.public.id
    route_table_id = aws_route_table.us-east-2a.id
}
